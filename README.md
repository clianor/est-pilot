# 파일럿 프로젝트 - 백엔드

### 셋팅
`est-pilot-front`를 레포에서 받아 프로젝트 내부에 위치하도록 둘 것

아래의 셋팅으로 하나의 프로젝트에서 모든 개발 모드 실행, 빌드, 실행이 가능
```json
"scripts": {
  "start": "cross-env NODE_ENV=production node dist/index.js",
  "prestart": "del /s /f /q dist && npm run build && npm run path && npm run build --prefix est-pilot-front",
  "start:dev": "concurrently \"npm run start:dev-server\" \"npm run start:dev-front\"",
  "start:dev-server": "nodemon --ignore est-pilot-front --exec ts-node -r tsconfig-paths/register src/index.ts",
  "start:dev-front": "npm run start --prefix est-pilot-front",
  "path": "tscpaths -p tsconfig.json -s ./src -o ./dist",
  "build": "tsc"
}
```

### .env 예시
```
DB_TYPE=sqlite
COOKIE_NAME=ssid
SECRET=ABCDEFG
REDIS_HOST=localhost
REDIS_PORT=6379
```

## 레디스 명령어 모니터링
```properties
$ redis-cli
$ monitor
```

### example
```
1605853392.721297 [0 127.0.0.1:57592] "get" "sess:YYmCQLo49OqunWFkXKoAj-DFwfyfk3tM"
1605853392.738400 [0 127.0.0.1:57592] "expire" "sess:YYmCQLo49OqunWFkXKoAj-DFwfyfk3tM" "86400"
1605853397.184327 [0 127.0.0.1:57592] "get" "sess:YYmCQLo49OqunWFkXKoAj-DFwfyfk3tM"
1605853397.184959 [0 127.0.0.1:57592] "expire" "sess:YYmCQLo49OqunWFkXKoAj-DFwfyfk3tM" "86400"
```