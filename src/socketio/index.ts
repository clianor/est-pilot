import { Server } from "http";
import { Server as SocketServer, Socket } from "socket.io";
import { createAdapter } from "socket.io-redis";
import { RedisClient } from "redis";
import sharedSession from "express-socket.io-session";

import { REDIS_HOST, REDIS_PORT } from "src/conf/constants";
import { clearRedisClient } from "src/utils/redisClearClients";
import redisSendClientList from "src/utils/redisSendClientList";
import { sessionMidleware } from "src/middleware/session";

const initSocketIo = (server: Server) => {
  const io = new SocketServer(server);

  // 레디스를 통해 데이터 전송
  const pubClient = new RedisClient({ host: REDIS_HOST, port: REDIS_PORT });
  const subClient = pubClient.duplicate();
  io.adapter(createAdapter({ pubClient, subClient }));

  // 소켓에 세션 연동
  io.use(sharedSession(sessionMidleware, { autoSave: true }));

  io.on("connection", (socket) => {
    const ip = socket.request.connection._peername.address.replace(
      "::ffff:",
      ""
    );
    socket.emit("hello", "nice to meet you!!");
    console.log("connect", ip, socket.id);

    socket.on("disconnect", () => {
      clearRedisClient(socket, pubClient);
    });

    socket.on("disconnecting", () => {
      const isClient = Array.from(
        (socket.adapter.sids as any).get(socket.id)
      ).includes("clients");

      if (isClient) {
        redisSendClientList(socket, pubClient);
      }
    });

    socket.on("join", async (room: string) => {
      await socket.join(room);

      if (room === "clients") {
        pubClient.hmset("clients", socket.id, ip);
        clearRedisClient(socket, pubClient);
        redisSendClientList(socket, pubClient);
      } else {
        redisSendClientList(socket, pubClient, true);
      }
    });

    // 디렉토리 이벤트
    createClientEvent(socket, "getDirectory");
    createClientEvent(socket, "reqFolders");

    // 이름 변경 이벤트
    createClientEvent(socket, "renameClient");

    // 파일 복사 이벤트
    createClientEvent(socket, "copyClient");

    // 파일 삭제 이벤트
    createClientEvent(socket, "deleteClient");

    // 파일 이동 이벤트
    createClientEvent(socket, "moveClient");

    // 파일 업로드 이벤트
    createEventForwarding(socket, "startUploadFile");
    createEventForwarding(socket, "reqUploadFile");
    createEventForwarding(socket, "finishUploadFile");

    // 파일 다운로드 이벤트
    createEventForwarding(socket, "startDownloadFile");
    createEventForwarding(socket, "reqDownloadFile");
    createEventForwarding(socket, "finishDownloadFile");
  });
};

const createClientEvent = (socket: Socket, event: string) => {
  const returendEventStr =
    "returned" + event.charAt(0).toUpperCase() + event.slice(1);

  socket.on(event, (clientId: string, data: any) => {
    // 유저인지 확인하기 위함
    const isNotClient = !socket.rooms.has("clients");
    const existUser = !!(socket.handshake as any).session?.user;

    // 유저인증을 받지 못하였을때
    if (isNotClient && !existUser) {
      socket.to(socket.id).emit("returnedError", "not authorized");
    }

    data["eventStr"] = event;
    data["returnedEventStr"] = returendEventStr;
    socket.to(clientId).emit(event, socket.id, data);
  });

  createEventForwarding(socket, returendEventStr);
};

const createEventForwarding = (socket: Socket, eventStr: string) => {
  socket.on(eventStr, (clientId: string, data: any) => {
    socket.to(clientId).emit(eventStr, socket.id, data);
  });
};

export default initSocketIo;
