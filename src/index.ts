import { main } from "./app";
import { PORT } from "./conf/constants";

main()
  .then((app) => {
    app.listen(PORT, () => {
      console.log(`server started on localhost:${PORT}`);
    });
  })
  .catch((error) => {
    console.error(error);
  });
