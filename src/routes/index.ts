import express from "express";

import usersRoutes from "src/api/users/users.controller";
import authRoutes from "src/api/auth/auth.controller";

const router = express.Router();

router.use("/users", usersRoutes);
router.use("/auth", authRoutes);

export default router;
