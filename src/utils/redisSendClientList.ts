import { RedisClient } from "redis";

const redisSendClientList = (
  socket: SocketIO.Socket,
  redisClient: RedisClient,
  from?: boolean
) => {
  redisClient.hgetall("clients", (_, obj) => {
    const clients = Array.from(
      (socket.adapter.rooms as any).get("clients") || []
    );

    if (!obj) {
      socket.emit("clientList", { clientList: [] });
    }

    const clientList = obj
      ? Object.entries(obj)
          .filter((client) => clients.includes(client[0]))
          .sort(function (a: string[], b: string[]): number {
            if (a[1] < b[1]) return -1;
            else if (a[1] > b[1]) return 1;
            return 0;
          })
      : [];
    const data = { clientList };

    from
      ? socket.emit("clientList", data)
      : socket.to("users").emit("clientList", data);
  });
};

export default redisSendClientList;
