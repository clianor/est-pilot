import { RedisClient } from "redis";

export const clearRedisClient = (
  socket: SocketIO.Socket,
  redisClient: RedisClient
) => {
  redisClient.hgetall("clients", (_, obj) => {
    const clients = Array.from(
      (socket.adapter.rooms as any).get("clients") || []
    );

    for (let key in obj) {
      if (!clients.includes(key)) {
        redisClient.hdel("clients", key);
      }
    }
  });
};
