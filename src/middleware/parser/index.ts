import { Router } from "express";
import parser from "body-parser";

const handleBodyRequestParsing = (router: Router) => {
  router.use(parser.urlencoded({ extended: true }));
  router.use(parser.json());
};

export default handleBodyRequestParsing;
