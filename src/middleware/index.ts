import handleCors from "./cors";
import handleBodyRequestParsing from "./parser";
import handleSession from "./session";
import handleFlash from "./flash";
import handlePassport from "./passport";

export default [
  handleCors,
  handleBodyRequestParsing,
  handleSession,
  handleFlash,
  handlePassport,
];
