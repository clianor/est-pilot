import { RequestHandler } from "express";
import { validate, ValidationError } from "class-validator";
import { plainToClass } from "class-transformer";

function validationMiddleware(
  type: any,
  skipMissingProperties = false
): RequestHandler {
  return (req, res, next) => {
    validate(plainToClass(type, req.body), { skipMissingProperties }).then(
      (errors: ValidationError[]) => {
        if (errors.length > 0) {
          const errMsgs = errors.map(
            (error: ValidationError) =>
              Object.values(error.constraints as any)[0]
          );
          errMsgs;

          res.status(400).json({ errMsgs: ["유효하지 않은 데이터입니다"] });
        } else {
          next();
        }
      }
    );
  };
}

export default validationMiddleware;
