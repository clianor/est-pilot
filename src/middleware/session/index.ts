import { Router } from "express";
import session, { SessionOptions } from "express-session";

import { COOKIE_NAME, SECRET_KEY } from "src/conf/constants";
import RedisStore from "src/middleware/redis";

const options: SessionOptions = {
  name: COOKIE_NAME,
  store: RedisStore(session),
  saveUninitialized: false,
  secret: SECRET_KEY,
  resave: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24, // 1 days
    httpOnly: false,
    sameSite: "lax",
    secure: false,
  },
};

export const sessionMidleware = session(options);

const handleSession = (router: Router) => {
  router.use(sessionMidleware);
};

export default handleSession;
