import { Router } from "express";
import cors from "cors";

const handleCors = (router: Router) => {
  router.use(cors({ origin: true, credentials: true }));
};

export default handleCors;
