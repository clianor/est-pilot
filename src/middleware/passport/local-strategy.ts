import passportLocal, { VerifyFunctionWithRequest } from "passport-local";
import { getCustomRepository } from "typeorm";
import argon2 from "argon2";

import UserRepository from "src/api/users/user.repository";

const LocalStrategy = passportLocal.Strategy;

/**
 * 로그인시 사용되는 유저 인증 로직
 *
 * @param {req} 요청 객체
 * @param {email} 유저의 이메일
 * @param {password} 유저의 패스워드
 * @param {done} 콜백 함수
 */
const verifyUser: VerifyFunctionWithRequest = async (
  _,
  email: string,
  password: string,
  done
) => {
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({
    select: ["id", "email", "password"],
    where: { email },
  });
  if (user) {
    try {
      return (await argon2.verify(user.password, password))
        ? done(null, user)
        : done(null, false, { message: "유효하지 않은 비밀번호입니다" });
    } catch (err) {
      return done(err, false, { message: "알 수 없는 에러가 발생했습니다." });
    }
  }
  return done(null, false, { message: "유저가 존재하지 않습니다." });
};

const localStrategy = new LocalStrategy(
  {
    usernameField: "email",
    session: true,
    passReqToCallback: true,
  },
  verifyUser
);

export default localStrategy;
