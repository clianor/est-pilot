import { Router } from "express";
import passport from "passport";
import { getCustomRepository } from "typeorm";

import localStrategy from "./local-strategy";
import UserRepository from "src/api/users/user.repository";
import { User } from "src/api/users/user.entity";

const handlePassport = (router: Router) => {
  router.use(passport.initialize());
  router.use(passport.session());

  passport.serializeUser((user: User, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id: string, done) => {
    const userRepository = getCustomRepository(UserRepository);
    userRepository
      .findById(id)
      .then((user) => {
        done(null, user);
      })
      .catch((err) => {
        done(err, null);
      });
  });

  passport.use(localStrategy);
};

export default handlePassport;
