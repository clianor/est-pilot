import Redis from "ioredis";
import connectRedis from "connect-redis";
import { REDIS_HOST, REDIS_PORT } from "src/conf/constants";

export default (session: any) => {
  const RedisStore = connectRedis(session);
  const redis = new Redis({
    host: REDIS_HOST,
    port: REDIS_PORT,
  });

  return new RedisStore({ client: redis, disableTouch: true });
};
