import { Router } from "express";
import flash from "connect-flash";

const handleFlash = (router: Router) => {
  router.use(flash());
};

export default handleFlash;
