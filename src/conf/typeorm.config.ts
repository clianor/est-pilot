import { ConnectionOptions } from "typeorm";

import { ROOT_DIR, NODE_ENV, ORM_DB_TYPE, NODE_ENV_STR } from "./constants";

const options: ConnectionOptions = {
  type: ORM_DB_TYPE,
  database:
    NODE_ENV_STR === "test"
      ? `${ROOT_DIR}/db/test.sqlite`
      : `${ROOT_DIR}/db/db.sqlite`,
  entities: [NODE_ENV ? "dist/**/*.entity.js" : "src/**/*.entity.ts"],
  logging: NODE_ENV_STR === "development",
  synchronize: !NODE_ENV,
};

export default options;
