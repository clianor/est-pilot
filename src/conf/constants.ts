import "dotenv/config";
import path from "path";

export const NODE_ENV_STR: string = process.env.NODE_ENV || "";
export const NODE_ENV: boolean =
  process.env.NODE_ENV === "production" ? true : false;
export const ROOT_DIR: string = path.resolve(__dirname, "../..");
export const PORT = process.env.PORT || 8000;
export const COOKIE_NAME = process.env.COOKIE_NAME || "cookie";
export const SECRET_KEY = process.env.SECRET as string;

export const ORM_DB_TYPE = process.env.DB_TYPE as
  | "sqlite"
  | "mariadb"
  | "mysql";

export const REDIS_HOST = process.env.REDIS_HOST || "localhost";
export const REDIS_PORT = parseInt(process.env.REDIS_PORT as string) || 6379;
