import { EntityRepository, Repository } from "typeorm";
import argon2 from "argon2";

import { User } from "./user.entity";

@EntityRepository(User)
export default class UserRepository extends Repository<User> {
  async createUser(email: string, password: string): Promise<User> {
    const user = this.create();
    user.email = email;
    user.password = await argon2.hash(password);
    return this.save(user).then((user) => {
      return user;
    });
  }

  findById(id: string): Promise<User | undefined> {
    return this.findOne({ where: { id } });
  }
}
