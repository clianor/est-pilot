import express from "express";

import usersService from "./users.service";

const router = express.Router();

/**
 * 모든 유저를 리턴합니다.
 *
 * @returns {User[]} 모든 유저를 리턴합니다.
 */
router.get("/", async (_, res, next) => {
  const users = await usersService.findAll().catch(next);
  return res.json(users);
});

/**
 * 하나의 유저를 리턴합니다.
 *
 * @param {id} 유저의 id값 입니다.
 * @returns {User} 하나의 유저를 리턴합니다.
 */
router.get("/:id", async (req, res, next) => {
  const { id } = req.params;
  const user = await usersService.findOne({ where: { id } }).catch(next);
  return res.json(user);
});

export default router;
