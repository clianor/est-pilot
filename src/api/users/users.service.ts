import { FindOneOptions, getCustomRepository } from "typeorm";

import { User } from "./user.entity";
import UserRepository from "./user.repository";

const findAll = (): Promise<User[]> => {
  const userRepository = getCustomRepository(UserRepository);
  return userRepository.find();
};

const findOne = (options?: FindOneOptions<User>): Promise<User | undefined> => {
  const userRepository = getCustomRepository(UserRepository);
  return userRepository.findOne(options);
};

export default {
  findOne,
  findAll,
};
