import chai from "chai";
import chaiHttp from "chai-http";
import { main } from "src/app";
import { COOKIE_NAME } from "src/conf/constants";
import { AuthLoginDto } from "../dto/auth-login.dto";
import { AuthRegisterDto } from "../dto/auth-register.dto";

chai.use(chaiHttp);
const expect = chai.expect;

describe("Auth Login", () => {
  let app: any;
  let authCookie: string;

  before(async function () {
    app = await main();
  });

  it("Before Login", (done) => {
    chai
      .request(app)
      .post("/api/auth/register")
      .send({
        email: "test@naver.com",
        password: "1234",
        passwordConfirm: "1234",
      } as AuthRegisterDto)
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(200);
        expect(res.body.success).to.have.true;
        done();
      });
  });

  it("Request Login", (done) => {
    chai
      .request(app)
      .post("/api/auth/login")
      .send({
        email: "test@naver.com",
        password: "1234",
      } as AuthLoginDto)
      .end((error, res) => {
        if (error) {
          done();
        }

        const cookies = res.header["set-cookie"].filter((cookie: string) =>
          cookie.includes(COOKIE_NAME)
        );

        expect(res).to.have.status(200);
        expect(res.body).to.include.keys("success");
        expect(res.body.success).to.be.true;
        expect(res.body).to.include.keys("user");
        expect(res.body.user).to.exist;
        expect(cookies).to.be.an("array").that.to.exist;

        authCookie = cookies[0];
        done();
      });
  });

  it("Login check", (done) => {
    chai
      .request(app)
      .get("/api/auth/me")
      .set("Cookie", authCookie)
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(200);
        expect(res.body).to.include.keys("user");
        expect(res.body.user).to.exist;
        done();
      });
  });
});
