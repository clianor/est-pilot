import chai from "chai";
import chaiHttp from "chai-http";
import { main } from "src/app";
import { AuthRegisterDto } from "../dto/auth-register.dto";

chai.use(chaiHttp);
const expect = chai.expect;

describe("Auth Register", () => {
  let app: any;
  before(async function () {
    app = await main();
  });

  it("No Data Send", (done) => {
    chai
      .request(app)
      .post("/api/auth/register")
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(400);
        done();
      });
  });

  it("Send without email", (done) => {
    chai
      .request(app)
      .post("/api/auth/register")
      .send({
        password: "1234",
        passwordConfirm: "1234",
      })
      .end((error, res) => {
        if (error) {
          done();
        }
        expect(res).to.have.status(400);
        expect(res.body).to.be.an("object");
        expect(res.body).to.be.eql({
          errMsgs: ["유효하지 않은 데이터입니다"],
        });
        done();
      });
  });

  it("Send without passwordConfirm", (done) => {
    chai
      .request(app)
      .post("/api/auth/register")
      .send({
        email: "oraclian@naver.com",
        password: "1234",
      })
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(400);
        expect(res.body).to.be.an("object");
        expect(res.body).to.be.eql({
          errMsgs: ["유효하지 않은 데이터입니다"],
        });
        done();
      });
  });

  it("Success Register", (done) => {
    chai
      .request(app)
      .post("/api/auth/register")
      .send({
        email: "oraclian@naver.com",
        password: "1234",
        passwordConfirm: "1234",
      })
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(200);
        expect(res.body.success).to.have.true;
        done();
      });
  });

  it("Users who already exist", (done) => {
    chai
      .request(app)
      .post("/api/auth/register")
      .send({
        email: "oraclian@naver.com",
        password: "1234",
        passwordConfirm: "1234",
      })
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(400);
        expect(res.body.success).to.be.false;
        expect(res.body).to.include.keys("errMsgs");
        expect(res.body.errMsgs).to.be.deep.equal([
          "이미 존재하는 이메일입니다",
        ]);
        done();
      });
  });

  it("Checking registered user", (done) => {
    chai
      .request(app)
      .get("/api/users/")
      .end((error, res) => {
        if (error) {
          done();
        }

        expect(res).to.have.status(200);
        expect(res.body).to.be.an("array");
        expect(
          res.body.filter(
            (user: AuthRegisterDto) => user.email === "oraclian@naver.com"
          )
        ).is.not.empty;
        done();
      });
  });
});
