import { getCustomRepository } from "typeorm";

import { User } from "src/api/users/user.entity";
import UserRepository from "src/api/users/user.repository";

const registerUser = (email: string, password: string): Promise<User> => {
  const userRepository = getCustomRepository(UserRepository);
  return userRepository.createUser(email, password);
};

export default {
  registerUser,
};
