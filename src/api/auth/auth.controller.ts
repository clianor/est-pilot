import express from "express";
import passport from "passport";
import { wrapError, UniqueViolationError } from "db-errors";

import authService from "./auth.service";
import validationMiddleware from "src/middleware/validation";
import { AuthRegisterDto } from "./dto/auth-register.dto";
import { AuthLoginDto } from "./dto/auth-login.dto";

const router = express.Router();

/**
 * 로그인
 *
 * @param {email} 유저의 이메일
 * @param {password} 유저의 패스워드
 */
router.post(
  "/login",
  validationMiddleware(AuthLoginDto),
  async (req: any, res, next) => {
    passport.authenticate("local", { failureFlash: true }, (_, user, info) => {
      // 동일 세션 중복 로그인 방지
      if (!!req.session.user) {
        // 401이 비록 비인증을 의미하지만 HTTP 표준에서는 미승인(unauthorized)이라 하고 있기에 401 에러 리턴
        res.status(401).json({
          success: false,
          user,
          errMsgs: ["이미 로그인된 사용자입니다"],
        });
        return;
      }

      delete user["password"];
      req.session.user = user;
      res.status(!!user ? 200 : 400).json({
        success: !!user,
        user,
        errMsgs: info ? [info?.message] : undefined,
      });
    })(req, res, next);
  }
);

/**
 * 회원가입
 *
 * @param {email} 유저의 이메일
 * @param {password} 유저의 패스워드
 * @param {passwordConfirm} 유저 패스워드 확인
 */
router.post(
  "/register",
  validationMiddleware(AuthRegisterDto),
  async (req, res, next) => {
    const { email, password, passwordConfirm } = req.body;
    if (password !== passwordConfirm) {
      return res.status(400).json({
        success: false,
        errMsgs: ["패스워드 확인 실패."],
      });
    }

    await authService.registerUser(email, password).catch((err) => {
      const error = wrapError(err);
      error instanceof UniqueViolationError &&
        req.flash("errors", "이미 존재하는 이메일입니다");
      return next;
    });

    const errMsgs = req.flash("errors");
    return res.status(!errMsgs.length ? 200 : 400).json({
      success: !errMsgs.length,
      errMsgs: errMsgs.length ? errMsgs : undefined,
    });
  }
);

/**
 * 로그아웃
 */
router.get("/logout", async (req: any, res) => {
  req.logout();
  req.session.destroy(() => {
    req.session;
  });
  return res.json({ success: true });
});

/**
 * 로그인 정보 가져오기
 */
router.get("/me", async (req: any, res) => {
  return res.json({
    user: req.session?.user || null,
  });
});

export default router;
