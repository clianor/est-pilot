import http from "http";
import path from "path";
import express from "express";
import { createConnection, getConnectionManager } from "typeorm";

import { applyMiddleware } from "./utils/applyMiddleware";
import typeormConfig from "./conf/typeorm.config";
import middlewareHandlers from "./middleware";
import routes from "./routes";
import initSocketIo from "./socketio";

export const main = async () => {
  try {
    await createConnection(typeormConfig);
  } catch (error) {
    // 이미 커넥션이 생성되어 있는 경우 기본 커넥션을 가져옵니다.
    if (error.name === "AlreadyHasActiveConnectionError") {
      await getConnectionManager().get("default");
    }
  }

  const app = express();
  const server = http.createServer(app);

  applyMiddleware(middlewareHandlers, app);
  app.use("/api", routes);
  initSocketIo(server);

  if (process.env.NODE_ENV === "production") {
    app.use(express.static("est-pilot-front/build"));

    app.get("*", (_, res) => {
      res.sendFile(path.resolve("./est-pilot-front", "build", "index.html"));
    });
  }

  return server;
};
